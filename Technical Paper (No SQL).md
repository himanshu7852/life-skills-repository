# NoSQL
## Introduction:
---
**NoSQL** is a type of database management system (DBMS) that is designed to handle and store large volumes of unstructured and semi-structured data. Unlike traditional relational databases that use tables with pre-defined schemas to store data, NoSQL databases use flexible data models that can adapt to changes in data structures and are capable of scaling horizontally to handle growing amounts of data.
## Types:
---
### NoSQL databases are generally classified into four main categories:
1. **Document databases:** These databases store data as semi-structured documents, such as JSON or XML, and can be queried using document-oriented query languages.
2. **Key-value stores:** These databases store data as key-value pairs, and are optimized for simple and fast read/write operations.
3. **Column-family stores:** These databases store data as column families, which are sets of columns that are treated as a single entity. They are optimized for fast and efficient querying of large amounts of data.
4. **Graph databases:** These databases store data as nodes and edges, and are designed to handle complex relationships between data.
   
## Pros and Cons of  NoSQL 
---

### Pros 
- **High scalability:** NoSQL systems use scale-out architecture which provides scalability when data volume or traffic grows.
- **Flexible storage:** Many different types of data, whether structured or semi-structured, can be stored and retrieved more easily.
- **Developer-friendly:** Developers find it easier to create various types of applications using NoSQL databases compared to using relational ones.
- **Minimum Downtime:** NoSQL databases take full advantage of the cloud to achieve this.
### Cons
- **Open-source:** There is no reliable standard for NoSQL yet. Two database systems are likely to be unequal.
-  **Management challenge:** Data management in NoSQL is much more complex than in a relational database.
  
## NoSQL database features:
---
Each NoSQL database has its own unique features. At a high level, many NoSQL databases have the following features:
- Flexible schemas
- Horizontal scaling
- Fast queries due to the data model
- Ease of use for developers

## Top NoSQL databases
---
1.  **[MongoDB](https://www.mongodb.com/)**: MongoDB is a general-purpose document-oriented database. It has a horizontal, scale-out architecture which means it can handle huge volumes of traffic and data thus making it **suitable for businesses having to store terabytes of data, or the database is going to be accessed by millions of users.** It is one of the most popular databases used by businesses recently.

2. **[MarkLogic](https://www.marklogic.com/)**: It is a multi-model NoSQL database **designed to handle complex data integration use cases such as large data sets with multiple models or in a fast-changing business environment.** It is compliant with ACID data principles making it consistent and reliable.

3. **[ScyllaDB](https://www.scylladb.com/)**: ScyllaDB is one of the fastest NoSQL databases out there. It should be **preferred when the business requirement is speed and cost-efficiency.** It is fully open-source and also provides great documentation support.

4. **[Apache Cassandra](https://cassandra.apache.org/)**: It is a free, open-source NoSQL database.  It is designed to be distributed across multiple servers, meaning that **the operations of a business will not collapse if there is an issue with a single server. It is popular with large enterprises that need a high level of consistency across their global business.**

5. **[Amazon DynamoDB](https://aws.amazon.com/dynamodb/)**: DynamoDB is a NoSQL database provided by Amazon as part of their Amazon Web Services suite. It sits completely in the cloud, meaning that the business does not need any hardware to use DynamoDB. It encrypts all data by default, and backups of data can be easily accessed. It is thus, **suitable when a business plans to launch a cloud-based product or if security and data loss prevention are important for the business.** 

6. **[Redis](https://redis.io/)**: This database is optimized for storing whole datasets in memory. Data is retrieved from the user device’s cache, thus, making it faster. It is a **great choice when the business requirement is to store and retrieve data in extremely short timeframes.**
   
## Summary
---
NoSQL databases provide a variety of benefits including flexible data models, horizontal scaling, lightning fast queries, and ease of use for developers. NoSQL databases come in a variety of types including document databases, key-values databases, wide-column stores, and graph databases.
## References 
---
- [https://www.mongodb.com/nosql-explained](https://www.mongodb.com/nosql-explained)
- [https://www.geeksforgeeks.org/introduction-to-nosql/](https://www.geeksforgeeks.org/introduction-to-nosql/)
- [https://www.couchbase.com/resources/why-nosql](https://www.couchbase.com/resources/why-nosql)
- [https://en.wikipedia.org/wiki/NoSQL](https://en.wikipedia.org/wiki/NoSQL)

