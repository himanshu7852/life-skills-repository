# Tiny Habits


# 1. Tiny Habits - BJ Fogg

## Question 1: Your takeaways from the video

- Design habits for the behaviour to achieve the outcome, not the outcome itself
- Have tiny habits which are designed to achieve an outcome. They take root in your system and compound the effect you put in to achieve the outcome
- To create a habit, one requires motivation, ability and a trigger

- Triggers can also have a cascading behaviour. Old behaviours can be used to trigger new behaviours

---
# 2. Tiny Habits by BJ Fogg - Core Message

## Question 2: Your takeaways from the video in as much detail as possible

- Behaviour = Motivation \* Ability \* Prompt

- Shrink the behaviour: Shrink habits to the tiniest possible version so that very little motivation is required to do it
  - Find a behaviour that is easy to do in 30 seconds or less
  - Find the tiniest habit with the biggest impact
- Identify a trigger

  - External Triggers

    - Too easy to ignore

  - Internal Triggers
    - Too easy to ignore
  - Action Triggers
    - Use the completion of one behaviour to trigger a new behaviour

- Grow your habit with some Shine
  - Start with something tiny and nurture it
  - The habit will naturally grow into a bigger habit
  - Shine: Authentic Pride, Celebrate when you achieve those tine habits every time you complete it
---
## Question 3: How can you use B = MAP to make making new habits easier?

- Start with a small habit which requires low motivation to complete, and we have the high ability to do it. Have behaviours which trigger those habits.
---
## Question 4: Why it is important to "Shine" or Celebrate after each successful completion of a habit?

- In order to keep one motivated and pumped up, one should celebrate the successful completion of tasks. As this small celebration keeps the person motivated and encourages him to add up some complexity to the task.
---
# 3. 1% Better Every Day Video

## Question 5 Your takeaways from the video (Minimum 5 points)

- Long-term changes can not be achieved in one day it is a time taking and step-by-step process, in order to achieve behaviour changes one needs to plan and simply work on little improvements every day.

- There are four stages of Habit Formation

  - Noticing
  - Wanting

  - Doing
  - Liking

- You can't take an action if you don't notice something

  - Implementation Action

    - Have the plan to implement the behaviour

    - Having clarity, and motivation without clarity of plan does nothing

- If you notice something, you should be 'wanting' to take an action on it

  - Design your environment to drive you towards achieving your habit

    - You want to read a book before bed? Keep the book on the pillow.

  - Make bad habits harder to achieve and good habits easier to achieve

- Learning how to start doing things is important

  - Two-minute rule:

    - If it takes two minutes, do it now

    - Any habit can be started in less than two minutes
    - Optimise for the beginning of the task

  - The best way to change long-term behaviour is with short-term feedback

  - Don't break the chain and never miss twice

- Like what you do

- If you can change your habits, you can change your life

# 4. Book Summary of Atomic Habits

## Question 6 Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes.

- The book suggested many ways of adapting any habit and making that habit your style of living, for doing this, the first step is to identify the habit and why you need that to adapt?, second step is to architect the path and generate a process to achieve your goal of adopting that habit, the third and last step is to reward your self with the outcomes.

## Question 7 Write about the book's perspective on how to make a good habit easier.

- Make it obvious, make it easy to trigger
- Keep fewer steps between you and the good behaviour

- Make it immediately satisfying

---

## Question 8 Write about the book's perspective on making a bad habit more difficult.

- Make it invisible, make it hard to trigger

- Keep more steps between you and the bad behaviour
- Make it unsatisfying

---

# 5. Reflection:

## Question 9: Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?

- I will read a book every day before sleeping

  - I can make it easier by keeping the book I am reading on my bedside table

  - I will reward myself with sleep after 20 minutes of reading

---

## Question 10: Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?

- Sleeping late
  - I will keep all the devices and distractions away from the bed after 9 p.m.
- A habit that I'd like to eliminate is of checking my phone regularly, even during work. I'd try removing the prompt like notification, which can be silenced by putting the phone in do not disturb mode. The other technique that I find useful is keeping my phone face down on my desk so that I'm less tempted to pick it up because then I won't see any screen flash caused by a new social media notification. The other thing I've found helpful is keeping my phone inside my bag, which doesn't remind me of social media at all. I also keep it deep inside my bag so that the process of taking it out takes one more step. I also find, hiding all social media apps in folders a bit helpful. Now it takes two or three steps more to get to them.

---