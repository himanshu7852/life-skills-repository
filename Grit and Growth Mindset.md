# Grit and Growth Mindset

## 1. Grit

### Question-1: Paraphrase (summarize) the video in a few lines. Use your own words.

* Grit, defined as passion and perseverance, is a strong predictor of success.
* IQ is not the sole factor in determining success; grit matters more.
* Grit is significant in various contexts, including education, military training, and sales.
* Building grit requires further research and the growth mindset shows promise.

### Question-2: What are your key takeaways from the video to take action on?
* Never be afraid to test yourself.
* Never take a failure as a permanent decision.
* Apply these principles in education and personal development.
---
## 2. Introduction to Growth Mindset

### Question-3: Paraphrase (summarize) the video in a few lines in your own words.
This vedio talks about the mindsets.
* Fixed mindset : 
1. Fixed mindset people believe that skill and intelligence are set whether you have them or not.
2. Fixed mindset people believe that skills are born.
* Growth mindset :
1. Growth mindset people believe that they can learn any skills and practice them to master them.
2. Growth mindset people believe skills can be learned.

### Question-4: What are your key takeaways from the video to take action on?
* Video talks about the importance of growth mindset and Fixed mindset people.
* Fixed mindset people believe that skills and intelligence are something you are born with and cannot be improved.
* Growth mindset people believe that skills are something that you can get good at by practicing and becoming the master of it.
* People with a growth mindset focus on learning and if they face failure they do not take it as a destiny for them.
* People with a growth mindset welcome constructive criticism and use that to learn from their mistakes.
---
## 3. Understanding Internal Locus of Control.

### Question-5: What is the Internal Locus of Control? What is the key point in the video?
Locus of Control refers to an individual's perception of the underlying
main causes of events in his/her life. Or, more simply:
Do you believe that your destiny is controlled by yourself or by external
forces?

---
## 3. How to build a Growth Mindset

### Question-6: Paraphrase (summarize) the video in a few lines in your own words.
The video talks about how we can develop our grit and growth mind. The first to believe in your abilities. The fundamental rule is to believe in your abilities and always try to learn new skills with a growth mindset. to believe create a mental structure.

### Question-7: What are your key takeaways from the video to take action on?
Some of the takeaways from the video are listed:-
* Believe in your abilities to figure out whatever things you are stuck with.
* Always question your mind and the thoughts are they negative or positive?
* If you are struggling with something do not get discouraged and try to reach the result.
---
## 4. Mindset - A MountBlue Warrior Reference Manual

### Question-8: What are one or more points that you want to take action on from the manual? (Maximum 3)
* I will wear confidence in my body. I will stand tall and sit straight.
* I will stay relaxed and focused no matter what happens.
* I will use the weapons of Documentation, Google, Stack Overflow, Github Issues and Internet before asking help from fellow warriors or look at the solution.