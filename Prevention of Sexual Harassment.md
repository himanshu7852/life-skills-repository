# Q.1. What kinds of behavior cause sexual harassment?

## Generally, we can categorize sexual harassment behavior as follows

### i. Verbal:

- Includes comments about clothing, a person's body, Gender-based jokes, requesting sexual favor or repeatedly asking a person for out.
- Sexual innuendos, threads, spreading rumors about the person's personal life or sexual life. Using Foul and obscene language.

### ii. Visual physical:

- Posters, drawings, pictures, screen savers, cartoons, and text of a sexual nature.

### iii. Physical:

- Includes Sexual assault, Impeding or blocking movement, and Inappropriate touching such as kissing, hugging, or rubbing. Sexual gesturing, looking unpleasantly.


# Q.2. What would you do in case you face or witness any incident or repeated incidents of such behavior?

## As a witness, I will do the following things:

- Stop an incident by simply interrupting it.

- Support someone who is being harassed and give suggestions.

- Report to the concerned person with proper documentation.